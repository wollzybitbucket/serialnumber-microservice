from flask import Flask, request, jsonify


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': 'Error',
        'data': ''
    }
    return jsonify(err), code


def get_serial_numebr_db(serial_number):
    return {
        'status': 'Ok',
        'data': {
            'id': 10,
            'serial_number': 735123,
            'type': 'card|template'
        }
    }


def create_serial_number_db(serial_number):
    return {
        'id': 10,
        'serial_number': 735123,
        'type': 'card|template',
        'parent': {
            'id': 10,
            'serial_number': 735123,
            'type': 'card|template'
        }
    }


@app.route('/serialnumber', methods=['GET'])
def get_serial_number():
    if 'serial_number' in request.args:
        return jsonify(get_serial_numebr_db(request.args.get('serial_number')))
    else:
        return make_error(400, 'serial_number is required.')


@app.route('/serialnumber', methods=['POST'])
def create_serial_number():
    data = request.json
    if data and 'serial_number' in data:
        return jsonify(create_serial_number_db(data['serial_number']))
    else:
        return make_error(400, 'serial_number is required')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)